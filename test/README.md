# Example for Project Structure for Modelling an Scenario

This is a template for the structure for the folders of a project.

Elements are described below

```
//:.
|   .gitignore				<--------- Ignore uploading of data and results. *.csv, *.txt, ...
|   LICENSE				<--------- License (no file extension)
|   README.md				<--------- Project Description
|   REQUIREMENTS.txt			<--------- List of dependencies
|   setup.py				<--------- Python package information
|   
+---data				<--------- Data folder
|   +---processed			<--------- Processed data (filtered, converted, rearranged, etc.).
|   \---raw				<--------- Raw data as originally obtained. Avoid manual manipulation of this data.
|           
+---docs				<--------- A default Sphinx project; see sphinx-doc.org for details.
|       
+---results
|   +---data				<--------- Results data as outuput from the models. Can be SQL, *.csv, *.txt, ... 
|   +---posprocessed			<--------- Filtered data, indicators, visualizations
|   \---reports				<--------- Automatically generated reports
|           
+---src					<--------- Folder for code. Scripts are to be located here
|   +---data_processing			<--------- Scripts for processing data (cleaning, filtering, converting).
|   |       __init__.py
|   |       
|   +---postprocessing			<--------- Results postprocessing. Visualization scripts. Report generation scripts
|   |       __init__.py
|   |       
|   \---scenario_demo			<--------- Main folder. Contains all files to be exported as package. 
|           common.py			<--------- Common function and utils folder. E.g., directory names, physics constants
|           main.py			<--------- Main script. Contains scenario definition, model parametrization.
|           scenario_model.py		<--------- Creates the structure of the model. A parametrizable method or class for evaluation.
|           __init__.py
|           
\---test				<--------- Unit tests, functional tests, 0-tests, configuration tests


```
