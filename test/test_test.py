'''
Created on 08.06.2021

@author: Fernando Penaherrera @UOL/OFFIS
'''
import unittest


class Test(unittest.TestCase):


    def test_sum(self):
        self.assertEqual(sum([1, 2, 3]), 6, "Should be 6")

    def test_sum_tuple(self):
        self.assertEqual(sum((1, 2, 2)), 6, "Should be 6")


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()