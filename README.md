# Multimodal Use of Flexibility in a District Grid

## Introduction
Within this scenario, flexibility of a heating, ventilation, and air conditioning (HVAC)  system is used to solve problems in the electric grid. Therefore, buildings including their HVAC are simulated. The flexibility of their HVAC is determined and translated in a uniform flexibility description. The flexibility of multiple buildings is aggregated and used when the grid simulation announces grid problems (e.g. voltage or current violations). The grid simulation includes load profiles of PV systems and households.

The grid simulation is performed using [pandapower](https://www.pandapower.org/). COHDA is used for the aggregation of flexibility. [Mango](https://gitlab.com/mango-agents/mango) is used as multiagent environment for COHDA. All simulators will be coupled using the co-simulation tool [mosaik](https://gitlab.com/mosaik).  

The scenario can be extended later on by adding electromobility, storage systems, marketing of flexibility and/or heat networks.

## Overview of the used simulators
![](docs/Multimodal_Use_of_Flexibility_in_a_District_Grid.png)*Overview of the simulators*

The scenario includes the following simulator

| Name | Short Description |
| ------ | ------ |
|  |  |
|  |  |
## Installation
